module.exports = {
    title: 'BLRevive',
    description: 'A wiki for BLRevive',
    base: '/wiki/',
    theme: 'book',
    themeConfig: {
        logo: '/assets/blrevive.ico',
        nav: [
            { text: 'Home', link: '/' },
            { 
                text: 'Users',
                items: [
                    { text: 'Getting Started', link: '/guides/user/getting-started' },
                    { text: 'Contribute', link: '/guides/user/contribution'},
                    {
                        text: 'Launcher',
                        items: [
                            { text: 'GUI', link: '/guides/launcher/gui.md' },
                            { text: 'CLI', link: '/guides/launcher/cli.md' }
                        ]
                    },
                ]
            },
            {
                text: 'Hosters',
                items: [
                    { text: 'Getting Started', link: '/guides/hosting' },
                    { 
                        text: 'Game Server',
                        items: [
                            { text: 'Basics', link: '/guides/hosting/game-server/' },
                            { text: 'CLI Parameter', link: '/guides/hosting/game-server/cli.md' },
                            { text: 'VM / Docker', link: '/guides/hosting/game-server/vm-docker.md'}
                        ]
                    },
                    { 
                        text: 'Super Server',
                        items: [
                            { text: 'Basics', link: '/guides/hosting/super-server/'},
                            { text: 'CLI Parameter', link: '/guides/hosting/super-server/cli.md'},
                            { text: 'Docker', link: '/guides/hosting/super-server/docker.md'},
                        ]
                    },
                ]
            },
            {
                text: 'Developers',
                items: [
                    { text: 'Getting Started', link: '/dev/' },
                    { text: 'Contribution Guideline', link: '/dev/contribution.md'},
                    { 
                        text: 'Frontend',
                        items: [
                            { text: 'Launcher', link: '/dev/launcher' },
                            { text: 'Website', link: '/dev/web'}
                        ]
                    },
                    {
                        text: 'Backend',
                        items: [
                            { text: 'Super Server', link: '/dev/super-server'},
                            { text: 'ZCAPI', link: '/dev/zcapi' },
                            { text: 'ServerUtil', link: '/dev/serverutil' },
                            { text: 'Proxy', link: '/dev/proxy'}
                        ]
                    },
                    {
                        text: 'Reverse Engineering',
                        items: [
                            { text: 'Getting Started', link: '/dev/re'}
                        ]
                    }
                ]
            }
        ],
        sidebar: 'auto',
        repo: 'https://gitlab.com/blrevive',
        repoLabel: 'GitLab',
        docsRepo: 'https://gitlab.com/blrevive/wiki',
        docsBranch: 'master',
        editLinks: true,
        editLinkText: 'Improve this page!'
    }
}