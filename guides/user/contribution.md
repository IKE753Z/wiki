# User Contribution

If you want to contribute to the repository here's a list of things you can do.

- Spread the word
- Help and Discuss with other users in our [Discord]()
- [Improve this Wiki](/guides/wiki/improve-wiki.md)
- [Host game servers](/guides/hosting/game-server)
- [Help with development](/dev)
- and **Play**