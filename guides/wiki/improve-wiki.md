# Improving the Wiki

## Report Issues

If you want to report an issue with the wiki you have to open an issue in
the [BLRevive/Wiki](), chose the template according to your issue from the list below 
and fill out the necessary parts.

- revision
    - false statements
    - broken links/images
    - outdated resources
    - misspells/typing
- improvement
    - readability
    - new informations
- bug
    - website not rendering correctly
    - problems with CI

## Change/Add Content

