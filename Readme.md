---
home: true
heroText: BLRevive
tagLine: BL:R for PC platform
actionText: Play BL:R on PC!
actionLink: /guides/user/getting-started
features:
- title: Play BL:R on PC!
  details: >
    Start playing BL:R again!
- title: Contribute
  details: > 
    Help this project with manpower or resources!
- title: Diving Deep
  details: >
    Help improving this project as developer!
---